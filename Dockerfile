FROM ubuntu:22.04

RUN apt-get update

RUN apt-get upgrade -y

RUN DEBIAN_FRONTEND=noninteractive

RUN TZ=America/Denver

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt install -y git

RUN apt install -y python3

RUN apt install -y python3-pip

RUN apt-get install -y ansible

RUN pip install ansible-core

RUN apt install -y git-lfs

RUN git lfs install --force

RUN apt install openconnect -y

#Install this to get SSH working on the Ubuntu container for Ansible.
RUN apt install sshpass -y

# Install this when you're troubhleshooting networking issues
RUN apt install iputils-ping -y

# libraby needed to interact with Palo Alto devices...
RUN pip install pan-os-python

# Moving the ansible.cfg
ADD conf/ansible.cfg /etc/ansible/ansible.cfg

# Adding the ansible collections
ADD conf/collections /etc/ansible/collections

# Adding the ansible inventories
ADD conf/inventories /etc/ansible/inventories

# Adding the ansible roles
ADD conf/roles /etc/ansible/roles

# Adding the ansible vaults
ADD conf/vaults /etc/ansible/vaults

#Install Palo Alto collection from Ansible Galaxy
#Not supported in the latest version of Ansible
# RUN ansible-galaxy collection install paloaltonetworks.panos

#Install Netbox collection from Ansible Galaxy
RUN ansible-galaxy collection install netbox.netbox

#Launcing and connecting to CUofCO GlobalProtect VPN. Uncomment the following lines if you want to use the VPN
#cat vpn/cuofco_vpn_pass.txt | sudo openconnect --background --protocol=gp 216.241.107.164 -u vanvuurenr --servercert pin-sha256:WOEVA5PKmoX4TbYpt1I4vCO3+t6oj7DzIhNvjesgX4U= --passwd-on-stdin
